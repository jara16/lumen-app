<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(['first_name' => 'Juan', 'last_name' => 'Rodriguez', 'email' => 'admin@admin.com', 'password' => \Illuminate\Support\Facades\Hash::make('123456789')]);
    }
}
