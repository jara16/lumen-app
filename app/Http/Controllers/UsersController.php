<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;

use App\User;

class UsersController extends Controller
{
    /**
     * Retrieve the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */

    public function index()
    {
      return User::all();
    }

    public function create(Request $request)
    {
      $this->validate($request, [
        'first_name' => 'required|string',
        'last_name' => 'required|string',
        'password' => 'required',
        'email' => 'required|email|unique:users'
      ]);

      $user = new User;
      $user->first_name = $request->input('first_name');
      $user->last_name  = $request->input('last_name');
      $user->email      = $request->input('email');
      $user->password   = Hash::make($request->input('password'));
      $user->save();

      return response()->json($user, 201);
    }

    public function update($id, Request $request)
    {
      $this->validate($request, [
        'first_name' => 'required|string',
        'last_name' => 'required|string',
        'password' => 'required'
      ]);

      $user = User::findOrFail($id);
      $user->first_name = $request->input('first_name');
      $user->last_name  = $request->input('last_name');
      $user->email      = $request->input('email');
      $user->password   = Hash::make($request->input('password'));
      $user->update();

      return response()->json($user, 200);
    }

    public function delete($id)
    {
      User::findOrFail($id)->delete();
      return response('Deleted Successfully', 200);
    }

    public function show($id)
    {
        return User::findOrFail($id);
    }
}